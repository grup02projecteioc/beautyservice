package beauty.service.service.repositori.Entitats;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="Salons")
public class Salons extends Usuari  {
    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "cifSalo")
    @Size(max=20)
    private String cifSalo;

    @NotNull
    @Column(name="nomSalo")
    @Size(max=45)   
    private String nomSalo;

    //Getters i Setters

    public String getNomSalo() {
        return nomSalo;
    }

    public String getCifSalo() {
        return cifSalo;
    }

    public void setCifSalo(String cifSalo) {
        this.cifSalo = cifSalo;
    }

    public void setNomSalo(String nomSalo) {
        this.nomSalo = nomSalo;
    }

}
