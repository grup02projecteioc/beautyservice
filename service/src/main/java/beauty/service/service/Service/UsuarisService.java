package beauty.service.service.Service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import beauty.service.service.repositori.CrudRepositoriUsuari;
import beauty.service.service.repositori.RepositoriUsuaris;
import beauty.service.service.repositori.Entitats.Usuari;

@Service
@Transactional
public class UsuarisService {
    @Autowired
    private CrudRepositoriUsuari repositoriCrud;
    @Autowired
    private RepositoriUsuaris repositori;

    //Mètode que retorna tots els usuaris 
    public Iterable<Usuari> listAll() {
        return repositoriCrud.findAll();
    }

    //Mètode que retorna un usuari pel seu ID
    public Optional<Usuari> getUsuari(int id) {
        return repositoriCrud.findById(id);
    }

    //Mètode que retorna l'últim codi assignat a la BD
    public int getUltimCodi() {
        return repositori.getCodi();
    }
    
}