package beauty.service.service.repositori.Entitats;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="Serveis")
public class Serveis implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "idServei")
    @Size(max=11)
    private int idServei;

    @NotNull
    @Column(name = "idTipusServei")
    @Size(max=11)   
    private int idTipusServei;

    //Getters i Setters
    public int getIdServei() {
        return idServei;
    }

    public void setIdServei(int idServei) {
        this.idServei = idServei;
    }

    public int getIdTipusServei() {
        return idTipusServei;
    }

    public void setIdTipusServei(int idTipusServei) {
        this.idTipusServei = idTipusServei;
    }

    
}
