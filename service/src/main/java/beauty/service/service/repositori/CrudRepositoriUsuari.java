package beauty.service.service.repositori;

import org.springframework.data.repository.CrudRepository;

import beauty.service.service.repositori.Entitats.Usuari;

//Interfície que crida a CrudRepository
public interface CrudRepositoriUsuari extends CrudRepository<Usuari, Integer> {

}