package beauty.service.service.repositori.Entitats;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="Reserves")
public class Reserves implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="idReserva")
    @Size(max=4)
    @NotNull
    private int idReserva;

    @NotNull
    @Column(name = "nifClient")
    @Size(max=9)
    private String nifCliente;

    @NotNull
    @Column(name = "idOferta")
    @Size(max=11)    
    private int idOferta;

    @NotNull
    @Column(name = "estat")
    @Size(max=4)    
    private int estat;

    //Getters i Setters
    public int getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }

    public String getNifCliente() {
        return nifCliente;
    }

    public void setNifCliente(String nifCliente) {
        this.nifCliente = nifCliente;
    }

    public int getIdOferta() {
        return idOferta;
    }

    public void setIdOferta(int idOferta) {
        this.idOferta = idOferta;
    }

    public int getEstat() {
        return estat;
    }

    public void setEstat(int estat) {
        this.estat = estat;
    }

    
}
