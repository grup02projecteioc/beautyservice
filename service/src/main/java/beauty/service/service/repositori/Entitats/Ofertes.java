package beauty.service.service.repositori.Entitats;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="Ofertes")
public class Ofertes implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="idOferta")
    @Size(max=11)
    @NotNull
    private int idOferta;

    @NotNull
    @Column(name = "nifSalo")
    @Size(max=45)
    private String nifSalo;

    @NotNull
    @Column(name = "idServicio")
    @Size(max=11)    
    private int idServicio;

    @Column(name = "descripcio")
    @Size(max=120)    
    private String descripcio;

    @NotNull
    @Column(name = "data")
    private Date data;

    @NotNull
    @Column(name = "tramHorari")
    @Size(max=45)   
    private String tramHorari;

    @NotNull
    @Column(name = "preu")   
    private double preu;

    //Getters i Setters
    public int getIdOferta() {
        return idOferta;
    }

    public void setIdOferta(int idOferta) {
        this.idOferta = idOferta;
    }

    public String getNifSalo() {
        return nifSalo;
    }

    public void setNifSalo(String nifSalo) {
        this.nifSalo = nifSalo;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getTramHorari() {
        return tramHorari;
    }

    public void setTramHorari(String tramHorari) {
        this.tramHorari = tramHorari;
    }

    public double getPreu() {
        return preu;
    }

    public void setPreu(double preu) {
        this.preu = preu;
    }

    
}
