package beauty.service.service.repositori.Entitats;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name="Clients")
public class Clients extends Usuari {
    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "nifClient")
    @Size(max=20)
    private String nifClient;

    //Getters i Setters
    public String getNifClient() {
        return nifClient;
    }

    public void setNifClient(String nifClient) {
        this.nifClient = nifClient;
    }
    
    
}