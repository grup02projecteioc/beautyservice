package beauty.service.service.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import beauty.service.service.Service.ClientsService;
import beauty.service.service.Service.UsuarisService;
import beauty.service.service.repositori.Entitats.Clients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
@RequestMapping("/clients")
public class ControladorClient {
    
    @Autowired
    private ClientsService clientsService;

    @Autowired
    private UsuarisService usuarisService;

    @RequestMapping(path="/add")
    public  @ResponseBody String addNewClient(@RequestParam String nifClient, @RequestParam String nom, @RequestParam String cognoms, 
        @RequestParam String telefon,  @RequestParam String mail,  @RequestParam String nomUsuari, @RequestParam String contrasenya,  @RequestParam String ciutat, @RequestParam String cp) {
        Clients client = new Clients();
        int lastCodi = usuarisService.getUltimCodi();
        client.setId(lastCodi + 1);
        client.setNifClient(nifClient);
        client.setNom(nom);
        client.setCognoms(cognoms);
        client.setTelefon(telefon);
        client.setMail(mail);
        client.setNomUsuari(nomUsuari);
        client.setContrasenya(contrasenya);
        client.setCiutat(ciutat);
        client.setCodiPostal(cp);

        return "Guardat";
    }

    @RequestMapping(path="/getAllClients")
    public @ResponseBody Iterable<Clients> getAllClients() {
        return clientsService.getAllClients();
    }

    @RequestMapping(path="/getClient")
    public @ResponseBody  Clients getClient(@RequestParam String nifClient) {
        return clientsService.getClientByNif(nifClient);
    }    
}
