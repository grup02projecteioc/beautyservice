package beauty.service.service.Service;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import beauty.service.service.repositori.CrudRepositoriClients;
import beauty.service.service.repositori.RepositoriClients;
import beauty.service.service.repositori.Entitats.Clients;

@Service
@Transactional
public class ClientsService {
    @Autowired
    private CrudRepositoriClients repositoriCrud;
    @Autowired
    private RepositoriClients repositori;

    //Mètode que retorna tots els salons 
    public Iterable<Clients> getAllClients() {
        return repositoriCrud.findAll();
    }
    
    //Mètode que guarda un saló a la BD
    public void save(Clients client) {
        repositoriCrud.save(client);
    }    
 
    //Mètode que retorna un saló pel seu cif
    public Clients getClientByNif(String nifClient) {
        return repositori.getClientByNif(nifClient);
    }
}
