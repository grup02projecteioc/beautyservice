package beauty.service.service.repositori;

import org.springframework.data.repository.CrudRepository;

import beauty.service.service.repositori.Entitats.Clients;

public interface CrudRepositoriClients extends CrudRepository<Clients, Integer> {
    
}
