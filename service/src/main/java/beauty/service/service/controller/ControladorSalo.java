package beauty.service.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import beauty.service.service.Service.SalonsService;
import beauty.service.service.repositori.Entitats.Salons;

@Controller
@RequestMapping("/salons")
public class ControladorSalo {

    @Autowired
    private SalonsService salonsService;

    @RequestMapping(path="/add")
    public  @ResponseBody String addNewSalo(@RequestParam String nifSalo, @RequestParam String nomSalo, @RequestParam String direccio, 
        @RequestParam String telefon,  @RequestParam String mail,  @RequestParam String contrasenya,  @RequestParam String ciutat, @RequestParam String cp) {
        Salons salo = new Salons();
        salo.setNomSalo(nomSalo);
        salo.setTelefon(telefon);
        salo.setMail(mail);
        salo.setContrasenya(contrasenya);
        salo.setCiutat(ciutat);
        salo.setCodiPostal(cp);
        salonsService.save(salo);

        return "Guardat";
    }

    @RequestMapping(path="/getAllSalons")
    public @ResponseBody Iterable<Salons> getAllSalons() {
        return salonsService.getAllSalons();
    }

    @RequestMapping(path="/getSalo")
    public @ResponseBody  Salons getSalo(@RequestParam String cifSalo) {
        return salonsService.getSaloByCif(cifSalo);
    }
}
